#!/bin/sh

set -e

ln ~/freebsd-setup/conf/zsh/freebsd-setup.zsh ~/.oh-my-zsh/custom/freebsd-setup.zsh
chsh -s /usr/local/bin/zsh
