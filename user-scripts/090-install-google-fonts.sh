#!/bin/sh

# Thanks to https://forums.freebsd.org/threads/61501/

set -e

wget https://github.com/google/fonts/archive/master.zip
unzip master.zip
rm master.zip
rm -rf .fonts
mv fonts-master ~/.fonts

mkdir -p ~/.config
ln -s ~/freebsd-setup/conf/fontconfig ~/.config/fontconfig

fc-cache -f
