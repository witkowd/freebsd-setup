#!/bin/sh

set -e

pkg install -y \
    ImageMagick \
    android-tools-adb \
    autoconf \
    automake \
    awscli \
    cabextract \
    ccrypt \
    cmake \
    cunit \
    curl \
    freetds \
    fzf \
    gawk \
    gcc \
    git \
    gmake \
    htop \
    hs-shellcheck \
    intltool \
    links \
    markdown \
    mercurial \
    mysql56-client \
    mysql56-server \
    rename \
    rsync \
    ruby \
    sakura \
    shtool \
    subversion \
    sudo \
    texinfo \
    the_silver_searcher \
    tree \
    v8 \
    wget \
    zenity \
    zsh
