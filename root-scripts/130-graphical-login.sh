#!/bin/sh

set -e

pkg install -y \
    xsm \
    xdm

sed -i.bak 's/ttyv8	"\/usr\/local\/bin\/xdm \-nodaemon"	xterm	off secure/ttyv8	"\/usr\/local\/bin\/xdm \-nodaemon"	xterm	on  secure/g' /etc/ttys
sed -i.bak 's/^DisplayManager\._0\./\!DisplayManager\._0\./g' /usr/local/etc/X11/xdm/xdm-config
