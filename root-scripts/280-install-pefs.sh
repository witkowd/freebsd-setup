#!/bin/sh

set -e

pkg install -y pefs-kmod

kldload pefs

touch /boot/loader.conf
{
  echo
  echo \# PEFS, added by freebsd-setup
  echo pefs_load=\"YES\"
  echo
} >> /boot/loader.conf


