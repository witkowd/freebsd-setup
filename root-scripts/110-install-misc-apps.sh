#!/bin/sh

set -e

pkg install -y \
    chromium \
    cmdwatch \
    cowsay \
    docker-freebsd \
    firefox \
    frotz \
    geeqie \
    gimp \
    inotify-tools \
    libdvdcss \
    libreoffice \
    p7zip \
    tex-formats \
    texlive-full \
    tmux \
    transmission-gtk \
    vlc \
    xpdf
